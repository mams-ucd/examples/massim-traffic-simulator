package hypermats.core.model;


public class Agent {
    private AgentDescription agent;
    private Action action = new Action("skip");

    public Agent(AgentDescription agent) {
        this.agent = agent;
    }

    public AgentDescription getAgent() {
        return agent;
    }

    public String getUrl(String host) {
        return "http://" + host + "/vehicle/" + agent.name;
    }
    
    public void setAction(Action action) {
        this.action = action;
    }

    public Action getAction() {
        return action;
    }
}
