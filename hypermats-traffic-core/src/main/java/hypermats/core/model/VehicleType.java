package hypermats.core.model;

public class VehicleType {
    public String url;
    public String type;
    public double length;

    public VehicleType(String url, String type, double length) {
        this.url = url;
        this.type = type;
        this.length = length;
    }
}
