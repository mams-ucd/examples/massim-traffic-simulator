package hypermats.core.model;

public class Link {
   public String url="";

   public Link(String url) {
    this.url = url;
   }

   public Link() {}
}
