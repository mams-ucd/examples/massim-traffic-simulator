package street.simulation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import hypermats.core.model.Link;
import street.model.StreetConfiguration;
import street.model.StreetDescription;
import street.model.Vehicle;

public class StreetSimulation {
    private ObjectMapper mapper = new ObjectMapper();
    private StreetConfiguration street;
    private Map<String, Vehicle> vehicles = new HashMap<>();
    private Lane lane;
    private String in;
    private String out;
    private String url;
    
    public StreetSimulation(StreetConfiguration street, String host) {
        this.street = street;
        this.url = "http://" + host + "/streets/" + street.id;
        this.lane = new Lane(street.length);
    }

    public String getUrl() {
        return url;
    }

    public StreetConfiguration getStreet() {
        return street;
    }

    public String getOut() {
        return out;
    }

    public String getIn() {
        return in;
    }

    public StreetDescription getStreetDescription() {
        StreetDescription description = new StreetDescription(street);
        for (Vehicle vehicle : vehicles.values()) {
            description.vehicles.add(vehicle.url);
        }
        description.in.add(new Link(in));
        description.out.add(new Link(out));
        return description;
    }

    public void setIn(String url) {
        this.in = url;
    }

    public void setOut(String url) {
        this.out = url;
    }

    // Simplest implementation
    // uniform speed
    // no collisions
    public LinkedList<Vehicle> step(int iteration) {
        return lane.step(iteration);
    }

    public void removeVehicle(Vehicle vehicle) {
        vehicles.remove(vehicle.name);
    }

    public Vehicle addVehicle(Vehicle vehicle) {
        vehicle.setSimulation(this);
        vehicles.put(vehicle.name, vehicle);
        lane.join(vehicle);
        return vehicle;
    }

    public String toHTML() {
        StringBuilder builder = new StringBuilder();
        builder.
            append("<style type='text/css'> td { min-width:30px;height:30px;text-align:center;vertical-align:middle; }</style>").
            append("<table border=1><tr><td>IN</td>");
        for (int i=0; i<vehicles.size(); i++) {
            builder.
                append("<td>").
                append(i).
                append("</td>");
        }
        builder.append("<td>OUT</td></tr><tr><td>").
            append(in).
            append("</td>");
        for (Vehicle vehicle : vehicles.values()) {
            builder.
                append("<td>").
                append(vehicle.toJson().toPrettyString()).
                append("</td>");
        }
        builder
            .append("<td>")
            .append(out)
            .append("</td></tr></table>");

        return builder.toString();
    }

    public JsonNode toJson() {
        ObjectNode description = mapper.createObjectNode();

        description.put("@id", getUrl());
        description.put("@context", "http://ont.astralanguage.com/2023/01/simulation");
        description.put("@type", "ContinousOneLaneStreet");
        description.put("length", street.length);
        description.put("lanes", 1);

        ArrayNode array = mapper.createArrayNode();
        for (Vehicle vehicle : vehicles.values()) {
            array.add(vehicle.toJson());
        }
        description.set("vehicles", array);

        return description;
    }

    public JsonNode toEmbeddedJson() {
        ObjectNode description = mapper.createObjectNode();

        description.put("@id", getUrl());
        description.put("@type", "ContinousOneLaneStreet");
        description.put("length", street.length);
        description.put("lanes", 1);

        return description;
    }
}
