package street.controller;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;

import hypermats.core.model.Action;
import street.model.Vehicle;
import street.service.RedirectsService;
import street.service.VehicleService;

@RestController
public class VehicleController {
    private Map<String, URI> redirects = new HashMap<>();

    @Autowired
    private VehicleService vehicleService;
    @Autowired
    private RedirectsService redirectsService;

    @Value("${server.port}")
    private int port;

    // @PatchMapping("/vehicle/{name}")
    // public ResponseEntity<JsonNode> updateAction(@PathVariable String name, @RequestBody JsonNode description) {
    //     if (description.has("perception") || !description.has("action")) {
    //         return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(description);
    //     }

    //     Vehicle vehicle = vehicleService.getVehicle(name);
    //     if (vehicle == null) {
    //         URI uri = redirectsService.get(name);
    //         if (uri == null) {
    //             return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    //         }
    //         return ResponseEntity
    //                 .status(HttpStatus.TEMPORARY_REDIRECT)
    //                 .header(HttpHeaders.LOCATION, uri.toASCIIString())
    //                 .build();
    //     }

    //     try {
    //         vehicle.getAgent().setAction(mapper.treeToValue(description.get("action"), Action.class));
    //     } catch (JsonProcessingException e) {
    //         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(description);
    //     }
    //     return ResponseEntity.status(HttpStatus.OK).build();
    // }

    @PutMapping("/vehicles/{name}/action")
    public ResponseEntity<Action> setAction(@PathVariable String name, @RequestBody Action action) {
        Vehicle vehicle = vehicleService.getVehicle(name);
        if (vehicle == null) {
            URI uri = redirects.get(name);
            if (uri == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
            return ResponseEntity
                    .status(HttpStatus.TEMPORARY_REDIRECT)
                    .header(HttpHeaders.LOCATION, uri.toASCIIString())
                    .build();
        }
        vehicle.agent.setAction(action);
        return ResponseEntity.status(HttpStatus.OK).body(action);
    }

    @GetMapping("/vehicles/{name}")
    public ResponseEntity<JsonNode> getVehicle(@PathVariable String name) {
        System.out.println("Name: " + name);
        Vehicle vehicle = vehicleService.getVehicle(name);
        if (vehicle == null) {
            URI uri = redirectsService.get(name);
            if (uri == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
            return ResponseEntity
                    .status(HttpStatus.TEMPORARY_REDIRECT)
                    .header(HttpHeaders.LOCATION, uri.toASCIIString())
                    .build();
        }

        return ResponseEntity.status(HttpStatus.OK).body(vehicle.toJson());
    }
}
