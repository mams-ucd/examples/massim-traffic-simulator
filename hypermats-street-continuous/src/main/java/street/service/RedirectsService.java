package street.service;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class RedirectsService {
    private Map<String, URI> redirects = new HashMap<>();

    public void put(String name, URI uri) {
        redirects.put(name, uri);
    }

    public URI get(String name) {
        return redirects.get(name);
    }

}
