import mams.*;
import mams.web.HttpResponse;
import com.fasterxml.jackson.databind.JsonNode;

agent Main extends mams.PassiveMAMSAgent {
    module Console console;
    module System system;
    module JSONBuilder builder;
    module JSONConverter converter;
    module HttpUtils httpUtils;

    rule +!main(list args) {
        MAMSAgent::!setup();
        MAMSAgent::!created("base");
        
        JsonNode state = builder.createObject();
        PassiveMAMSAgent::!itemResource("state", state);

        query(artifact("state", string qualified_name, cartago.ArtifactId id));
        cartago.operation(id, getUri(string uri));
        cartago.println("Webhook: "  + uri);
    }

    rule $cartago.signal(string source_artifact_name, update(JsonNode node)) {
        cartago.println("source: " + source_artifact_name);
        cartago.println("UPDATE: " + node);

        JsonNode action = converter.getNode(node,"/action");
        string url = converter.valueAsString(action, "@id");
        JsonNode state = converter.getNode(node,"/state");

        JsonNode nextAction = builder.createObject();
        if (converter.compare(state, "/speed", 0)) {
            builder.setProperty(nextAction, "id", "accelerate");
        } else {
            builder.setProperty(nextAction, "id", "skip");
        }

        !put(url, builder.toJsonString(nextAction), HttpResponse response);
        if (httpUtils.hasCode(response, 200)) {
            cartago.println("Yipee!");
        }
    }

}
