package hypermats;

import java.lang.reflect.Field;

import com.fasterxml.jackson.databind.JsonNode;

import astra.core.Module;

public class JsonNodeAccessor extends Module {
    @TERM public JsonNode getJsonNode(Object object, String fieldName) {
        try {
            Field field = object.getClass().getField(fieldName);
            return (JsonNode) field.get(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
