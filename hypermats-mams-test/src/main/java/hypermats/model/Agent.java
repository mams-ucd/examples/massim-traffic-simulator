package hypermats.model;

import mams.utils.Identifier;
import com.fasterxml.jackson.databind.JsonNode;

public class Agent {
    @Identifier
    public String name;
    public JsonNode init;  
}
