/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;

import mams.*;
import mams.web.HttpResponse;
import com.fasterxml.jackson.databind.JsonNode;

public class Main extends ASTRAClass {
	public Main() {
		setParents(new Class[] {mams.PassiveMAMSAgent.class});
		addRule(new Rule(
			"Main", new int[] {12,9,12,28},
			new GoalEvent('+',
				new Goal(
					new Predicate("main", new Term[] {
						new Variable(Type.LIST, "args",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {12,27,21,5},
				new Statement[] {
					new ScopedSubgoal(
						"Main", new int[] {13,8,21,5},
						"MAMSAgent",
						new Goal(
							new Predicate("setup", new Term[] {})
						)
					),
					new ScopedSubgoal(
						"Main", new int[] {14,8,21,5},
						"MAMSAgent",
						new Goal(
							new Predicate("created", new Term[] {
								Primitive.newPrimitive("base")
							})
						)
					),
					new Declaration(
						new Variable(new ObjectType(JsonNode.class), "state"),
						"Main", new int[] {15,8,21,5},
						new ModuleTerm("builder", new ObjectType(com.fasterxml.jackson.databind.JsonNode.class),
							new Predicate("createObject", new Term[] {}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((mams.JSONBuilder) intention.getModule("Main","builder")).createObject(
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((mams.JSONBuilder) visitor.agent().getModule("Main","builder")).createObject(
									);
								}
							}
						)
					),
					new ScopedSubgoal(
						"Main", new int[] {16,8,21,5},
						"PassiveMAMSAgent",
						new Goal(
							new Predicate("itemResource", new Term[] {
								Primitive.newPrimitive("state"),
								new Variable(new ObjectType(JsonNode.class), "state")
							})
						)
					),
					new Query(
						"Main", new int[] {18,8,18,78},
						new Predicate("artifact", new Term[] {
							Primitive.newPrimitive("state"),
							new Variable(Type.STRING, "qualified_name",false),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
						})
					),
					new ModuleCall("cartago",
						"Main", new int[] {19,8,19,49},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("getUri", new Term[] {
								new Variable(Type.STRING, "uri",false)
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("Main","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"Main", new int[] {20,8,20,43},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("Webhook: "),
								new Variable(Type.STRING, "uri")
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("Main","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {23,9,23,78},
			new ModuleEvent("cartago",
				"$cse",
				new Predicate("signal", new Term[] {
					new Variable(Type.STRING, "source_artifact_name",false),
					new Funct("update", new Term[] {
						new Variable(new ObjectType(JsonNode.class), "node",false)
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.lang.Cartago) agent.getModule("Main","cartago")).signal(
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {23,77,42,5},
				new Statement[] {
					new ModuleCall("cartago",
						"Main", new int[] {24,8,24,58},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("source: "),
								new Variable(Type.STRING, "source_artifact_name")
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("Main","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"Main", new int[] {25,8,25,42},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("UPDATE: "),
								new Variable(new ObjectType(JsonNode.class), "node")
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("Main","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new Declaration(
						new Variable(new ObjectType(JsonNode.class), "action"),
						"Main", new int[] {27,8,42,5},
						new ModuleTerm("converter", new ObjectType(com.fasterxml.jackson.databind.JsonNode.class),
							new Predicate("getNode", new Term[] {
								new Variable(new ObjectType(JsonNode.class), "node"),
								Primitive.newPrimitive("/action")
							}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((mams.JSONConverter) intention.getModule("Main","converter")).getNode(
										(com.fasterxml.jackson.databind.JsonNode) intention.evaluate(predicate.getTerm(0)),
										(java.lang.String) intention.evaluate(predicate.getTerm(1))
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((mams.JSONConverter) visitor.agent().getModule("Main","converter")).getNode(
										(com.fasterxml.jackson.databind.JsonNode) visitor.evaluate(predicate.getTerm(0)),
										(java.lang.String) visitor.evaluate(predicate.getTerm(1))
									);
								}
							}
						)
					),
					new Declaration(
						new Variable(Type.STRING, "url"),
						"Main", new int[] {28,8,42,5},
						new ModuleTerm("converter", Type.STRING,
							new Predicate("valueAsString", new Term[] {
								new Variable(new ObjectType(JsonNode.class), "action"),
								Primitive.newPrimitive("@id")
							}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((mams.JSONConverter) intention.getModule("Main","converter")).valueAsString(
										(com.fasterxml.jackson.databind.JsonNode) intention.evaluate(predicate.getTerm(0)),
										(java.lang.String) intention.evaluate(predicate.getTerm(1))
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((mams.JSONConverter) visitor.agent().getModule("Main","converter")).valueAsString(
										(com.fasterxml.jackson.databind.JsonNode) visitor.evaluate(predicate.getTerm(0)),
										(java.lang.String) visitor.evaluate(predicate.getTerm(1))
									);
								}
							}
						)
					),
					new Declaration(
						new Variable(new ObjectType(JsonNode.class), "state"),
						"Main", new int[] {29,8,42,5},
						new ModuleTerm("converter", new ObjectType(com.fasterxml.jackson.databind.JsonNode.class),
							new Predicate("getNode", new Term[] {
								new Variable(new ObjectType(JsonNode.class), "node"),
								Primitive.newPrimitive("/state")
							}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((mams.JSONConverter) intention.getModule("Main","converter")).getNode(
										(com.fasterxml.jackson.databind.JsonNode) intention.evaluate(predicate.getTerm(0)),
										(java.lang.String) intention.evaluate(predicate.getTerm(1))
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((mams.JSONConverter) visitor.agent().getModule("Main","converter")).getNode(
										(com.fasterxml.jackson.databind.JsonNode) visitor.evaluate(predicate.getTerm(0)),
										(java.lang.String) visitor.evaluate(predicate.getTerm(1))
									);
								}
							}
						)
					),
					new Declaration(
						new Variable(new ObjectType(JsonNode.class), "nextAction"),
						"Main", new int[] {31,8,42,5},
						new ModuleTerm("builder", new ObjectType(com.fasterxml.jackson.databind.JsonNode.class),
							new Predicate("createObject", new Term[] {}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((mams.JSONBuilder) intention.getModule("Main","builder")).createObject(
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((mams.JSONBuilder) visitor.agent().getModule("Main","builder")).createObject(
									);
								}
							}
						)
					),
					new If(
						"Main", new int[] {32,8,42,5},
						new ModuleFormula("converter",
							new Predicate("compare", new Term[] {
								new Variable(new ObjectType(JsonNode.class), "state"),
								Primitive.newPrimitive("/speed"),
								Primitive.newPrimitive(0)
							}),
						new ModuleFormulaAdaptor() {
								public Formula invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((mams.JSONConverter) visitor.agent().getModule("Main","converter")).compare(
										(com.fasterxml.jackson.databind.JsonNode) visitor.evaluate(predicate.getTerm(0)),
										(java.lang.String) visitor.evaluate(predicate.getTerm(1)),
										(int) visitor.evaluate(predicate.getTerm(2))
									);
							}
						}
							),
						new Block(
							"Main", new int[] {32,50,34,9},
							new Statement[] {
								new ModuleCall("builder",
									"Main", new int[] {33,12,33,63},
									new Predicate("setProperty", new Term[] {
										new Variable(new ObjectType(JsonNode.class), "nextAction"),
										Primitive.newPrimitive("id"),
										Primitive.newPrimitive("accelerate")
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return true;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((mams.JSONBuilder) intention.getModule("Main","builder")).setProperty(
												(com.fasterxml.jackson.databind.JsonNode) intention.evaluate(predicate.getTerm(0)),
												(java.lang.String) intention.evaluate(predicate.getTerm(1)),
												(java.lang.String) intention.evaluate(predicate.getTerm(2))
											);
										}
									}
								)
							}
						),
						new Block(
							"Main", new int[] {34,15,42,5},
							new Statement[] {
								new ModuleCall("builder",
									"Main", new int[] {35,12,35,57},
									new Predicate("setProperty", new Term[] {
										new Variable(new ObjectType(JsonNode.class), "nextAction"),
										Primitive.newPrimitive("id"),
										Primitive.newPrimitive("skip")
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return true;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((mams.JSONBuilder) intention.getModule("Main","builder")).setProperty(
												(com.fasterxml.jackson.databind.JsonNode) intention.evaluate(predicate.getTerm(0)),
												(java.lang.String) intention.evaluate(predicate.getTerm(1)),
												(java.lang.String) intention.evaluate(predicate.getTerm(2))
											);
										}
									}
								)
							}
						)
					),
					new Subgoal(
						"Main", new int[] {38,8,42,5},
						new Goal(
							new Predicate("put", new Term[] {
								new Variable(Type.STRING, "url"),
								new ModuleTerm("builder", Type.STRING,
									new Predicate("toJsonString", new Term[] {
										new Variable(new ObjectType(JsonNode.class), "nextAction")
									}),
									new ModuleTermAdaptor() {
										public Object invoke(Intention intention, Predicate predicate) {
											return ((mams.JSONBuilder) intention.getModule("Main","builder")).toJsonString(
												(com.fasterxml.jackson.databind.JsonNode) intention.evaluate(predicate.getTerm(0))
											);
										}
										public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
											return ((mams.JSONBuilder) visitor.agent().getModule("Main","builder")).toJsonString(
												(com.fasterxml.jackson.databind.JsonNode) visitor.evaluate(predicate.getTerm(0))
											);
										}
									}
								),
								new Variable(new ObjectType(HttpResponse.class), "response",false)
							})
						)
					),
					new If(
						"Main", new int[] {39,8,42,5},
						new ModuleFormula("httpUtils",
							new Predicate("hasCode", new Term[] {
								new Variable(new ObjectType(HttpResponse.class), "response"),
								Primitive.newPrimitive(200)
							}),
						new ModuleFormulaAdaptor() {
								public Formula invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((mams.HttpUtils) visitor.agent().getModule("Main","httpUtils")).hasCode(
										(mams.web.HttpResponse) visitor.evaluate(predicate.getTerm(0)),
										(int) visitor.evaluate(predicate.getTerm(1))
									);
							}
						}
							),
						new Block(
							"Main", new int[] {39,45,41,9},
							new Statement[] {
								new ModuleCall("cartago",
									"Main", new int[] {40,12,40,37},
									new Predicate("println", new Term[] {
										Primitive.newPrimitive("Yipee!")
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return false;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.Cartago) intention.getModule("Main","cartago")).auto_action(intention,evaluate(intention,predicate));
										}
										public boolean suppressNotification() {
											return true;
										}
									}
								)
							}
						)
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("console",astra.lang.Console.class,agent);
		fragment.addModule("system",astra.lang.System.class,agent);
		fragment.addModule("builder",mams.JSONBuilder.class,agent);
		fragment.addModule("converter",mams.JSONConverter.class,agent);
		fragment.addModule("httpUtils",mams.HttpUtils.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new TestSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new Main().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
