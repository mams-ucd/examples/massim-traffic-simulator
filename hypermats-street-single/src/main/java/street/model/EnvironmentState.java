package street.model;

import java.util.LinkedList;

import hypermats.core.model.Action;

public class EnvironmentState {
    public VehicleState perceptions;
    public LinkedList<Action> actions = new LinkedList<>();
    
    public EnvironmentState(VehicleState perceptions) {
        this.perceptions = perceptions;

        // Build affordance list (here for now)
        actions.add(new Action("skip"));
        if (perceptions.speed > 0) actions.add(new Action("stop"));
        if (perceptions.speed == 0) actions.add(new Action("start"));
    }
}
