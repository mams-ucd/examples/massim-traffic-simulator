package street.model;

import hypermats.core.model.BasicState;

public class VehicleState extends BasicState {
    public int at;
    public int length;
    public int speed;
}
