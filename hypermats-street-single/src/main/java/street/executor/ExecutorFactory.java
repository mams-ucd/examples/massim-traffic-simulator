package street.executor;

import java.util.HashMap;
import java.util.Map;

public class ExecutorFactory {
    private static Map<String, Executor> executors = new HashMap<>();

    static {
        register(new SkipExecutor());
        register(new StopExecutor());
        register(new StartExecutor());
    }

    public static void register(Executor executor) {
        executors.put(executor.getActionId(), executor);
    }
    
    public static Executor get(String type) {
        return executors.get(type);
    }
}
