# HyperMATS: Hypermedia-based Multi-Agent Traffic Simulator

This project is a prototype implementation of a Hypermedia MAS Traffic Simulator. It is implemented as a set of microservices. Environment microservices implement 
streets and junctions as hypermedia resources. Agents connect to the system by registering with one of these resources. Once connected the agent resides within that
resource until it is required to exit.  For example, in a street microservice, the agent is required to transfer to a connected junction resource when it reaches 
the end of the street. Similarly, the agent is transfered to a street microservice when it exits the junction.

Currently, the streets are implemented as one-way streets, and all entrances / exits for junctions are given equal priority. Additional microservices will be created
over time that offer different models (e.g. major/minor t-junction, roundabout, two way streets, ...)

The simulation consists of five services:

* Street Microservice: the street simulation
* Junction Microservice: the junction simulation
* Agent Microservice: A simple agent implementation that is able to traverse the road network.
* Clock Microservice: Implements a global clock
* Configuration Microservice: Sets up the simulation based on a given configration


## Getting started

* Use maven to compile the code (mvn install)
* Use docker-compose to build and run the system (docker-compose up --build)
* Send a PUT request to http://localhost:8082/configuration to initialise a simulation

## Example PUT Request Content
```
{ "services" : [
    {
      "url" : "http://street:8080/streets",
      "type" : "street",
      "instances": [ { "id" : "S1", "length" : 3 } ]
    }, {
      "url" : "http://junction:8081/junctions",
      "type" : "junction",
      "instances" : [ { "id": "J1", "inLinks":2, "outLinks":2 } ]
    }, {
        "url" : "http://agent:8083/agents",
        "type" : "agent",
        "instances" : [ { "name" : "rem", "init" : { "url" : "http://street:8080/streets/S1/in/0"}}]
    }
  ],
  "links" : [
    { "out" : { "type": "street", "id": "S1" }, "in" : {"type": "junction", "id" : "J1" } },
    { "out" : { "type": "junction", "id" : "J1" }, "in" : { "type" : "street", "id" : "S1" } }
  ],
  "clock" : {
      "url" : "http://clock:9000/registry"
  }
}
```
