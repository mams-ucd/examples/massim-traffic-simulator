import { LineString } from 'ol/geom';
import {lineString, lineIntersect} from '@turf/turf';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point.js';

/** 
 * Class that takes in the road and junction data (id, length etc)
 * and 
 */
export class Convertor {

    constructor(roads, junctions) {
        this.roads = roads;
        this.junctions = junctions;
    }

    getCoordinates(id, length) {
        if (length) {
            for (let i = 0; i<this.roads.length; i++) {
                let road = this.roads[i];
                if (road.id == id) {
                    //Bingo
                    //how far along are we?
                    let ls = new LineString(road.coordinates, "XY");
                    let roadLength = road.length;
                    let fract = length/roadLength;
                    return ls.getCoordinateAt(fract);
                }
            }
        } else {
            for (let i = 0; i<this.junctions.length; i++) {
                let junct = this.junctions[i];
                if (junct.id == id) {
                    //Bingo
                    return junct.coordinates;
                }
            }
        }
    }
}
