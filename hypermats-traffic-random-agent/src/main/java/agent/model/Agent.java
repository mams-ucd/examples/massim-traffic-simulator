package agent.model;

import java.net.URI;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;

import hypermats.core.model.Action;
import hypermats.core.model.AgentDescription;

public class Agent {
    private String name;
    private URI uri;
    // private static Random random = new Random();
    
    public Agent(String name) {
        this.name = name;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public AgentDescription getAgentDescription(String host) {
        AgentDescription description = new AgentDescription();
        description.name = name;
        description.webhook = "http://"+host+"/agents/"+name;
        return description;
    }

    public void execute(JsonNode state) {
        System.out.println("--------------------------"+name+"--------------------------");
        System.out.println(state);
        JsonNode actions = state.get("actions");
        JsonNode perceptions = state.get("perceptions");

        // Default action == 0 (skip)
        int action = 0;
        if (perceptions.get("type").asText().equals("junction")) {
            // selecting first turn action...
            action = 1;
        } else if (perceptions.get("type").asText().equals("street")) {
            if (perceptions.get("speed").asInt() == 0) {
                // vehicle is stopped, so start it moving...
                action = 1;
            }
        }
        System.out.println("[" + name + "] Selected: " + actions.get(action));
        String url = uri.toASCIIString()+"/action";

        System.out.println("[" + name + "] Url: " + url);
        RestTemplate template = new RestTemplate();
        HttpEntity<JsonNode> request = new HttpEntity<JsonNode>(actions.get(action));
        ResponseEntity<Action> response = template.exchange(url, HttpMethod.PUT, request, Action.class);
        System.out.println(response.getStatusCode());
        while (response.getStatusCode() == HttpStatus.TEMPORARY_REDIRECT) {
            uri = response.getHeaders().getLocation();
            url = uri.toASCIIString()+"/action";

            System.out.println("[" + name + "] New Url: " + url);
            response = template.exchange(url, HttpMethod.PUT, request, Action.class);
            System.out.println(response.getStatusCode());
        }    
    }
}
