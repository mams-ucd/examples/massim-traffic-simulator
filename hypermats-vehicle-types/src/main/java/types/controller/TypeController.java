package types.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import hypermats.core.model.VehicleType;

@RestController
public class TypeController {
    private ObjectMapper mapper = new ObjectMapper();
    private Map<String, VehicleType> types = new HashMap<>();

    {
    }

    @Value("${server.port}")
    private int port;
    
    @GetMapping("/types")
    public ResponseEntity<JsonNode> getTypes() {
        // Lazy Initialization of demo types...
        if (types.isEmpty()) {
            try {
                types.put("basic", new VehicleType("http://"+getHost()+"/types/basic", "basic", 1.0));
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }

        ArrayNode array = mapper.createArrayNode();
        try {
            for (VehicleType type : types.values()) {
                ObjectNode node = mapper.createObjectNode();
                node.put("@id", "http://" + getHost() + "/vehicles/" + type.type);
                node.put("@context", "http://ont.astralanguage.com/2023/01/simulation");
                node.put("@type", "Vehicle");
                node.put("type", type.type);
                node.put("length", type.length);
                array.add(node);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(array);
    }

    @GetMapping("/types/{type}")
    public ResponseEntity<JsonNode> getType(@PathVariable String type) {
        System.out.println("Request for type: " + type);
        // Lazy Initialization of demo types...
        if (types.isEmpty()) {
            try {
                types.put("basic", new VehicleType("http://"+getHost()+"/types/basic", "basic", 1.0));
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }

        try {
            VehicleType vehicleType = types.get(type);
            ObjectNode node = mapper.createObjectNode();
            node.put("@id", "http://" + getHost() + "/types/" + vehicleType.type);
            node.put("@context", "http://ont.astralanguage.com/2023/01/simulation");
            node.put("@type", "Vehicle");
            node.put("type", vehicleType.type);
            node.put("length", vehicleType.length);
            return ResponseEntity.status(HttpStatus.OK).body(node);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private String getHost() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostAddress() + ":" + port;
    }
}
